import random

# Daftar dialog NPC
dialogs = [
    "Halo, apa kabar?",
    "Apakah Anda sedang mencari sesuatu?",
    "Bagaimana keadaan cuaca hari ini?",
    "Apakah Anda pernah bermain game ini sebelumnya?",
    "Anda punya pertanyaan lain?",
]

def get_random_dialog():
    # Memilih dialog secara acak
    return random.choice(dialogs)

def main():
    # Menjalankan skrip NPC
    print("NPC: Selamat datang di dunia permainan!")
    while True:
        player_input = input("Player: ")
        if player_input.lower() == "exit":
            print("NPC: Sampai jumpa!")
            break
        else:
            npc_dialog = get_random_dialog()
            print("NPC:", npc_dialog)

if __name__ == '__main__':
    main()
